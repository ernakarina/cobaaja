import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompsatuComponent } from './compsatu.component';

describe('CompsatuComponent', () => {
  let component: CompsatuComponent;
  let fixture: ComponentFixture<CompsatuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompsatuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompsatuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
